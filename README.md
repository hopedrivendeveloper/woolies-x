# Woolies X

This repository is an attempt to solve coding challnge from WooliesX.

## Assumptions made
- User token is provided as configuration parameter, either via appsettings file or environment variables 
- Test tool http://dev-wooliesx-recruitment.azurewebsites.net/exercise tries to access apps urls with `//` in front. For application to run on any platform, custom middleware has been used to rewrite path.

## How to run 
- navigate to `src` folder
- `docker build -t woolies-x-challenge .`
- `docker run -p 80:80 woolies-x-challenge` - will run app in production mode (vailable on port 80)
- `docker run -p 80:80 -e ASPNETCORE_ENVIRONMENT=Development woolies-x-challenge` - will run app in development mode (vailable on port 80)
- `docker run -p 80:80 -e User__Token=custom-token woolies-x-challenge` - to set user token

For more parameters refer to https://docs.docker.com/engine/reference/commandline/run/