using System.Text.Json;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using WooliesX.Common;
using Xunit;

namespace WooliesX.Api.Tests
{
    public class UserControllerTests : HostSetup
    {
        [Fact]
        public async Task Get_ReturnsUserInformation()
        {
            var apiResponse = await ApiClient.GetAsync("/user");
            
            apiResponse.StatusCode.Should().Be(StatusCodes.Status200OK);
            var responseObject = JsonSerializer.Deserialize<User>(await apiResponse.Content.ReadAsStringAsync(), new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });
            responseObject.Name.Should().Be("This comes from configuration.");
            responseObject.Token.Should().Be("random-token");
        }
    }
}