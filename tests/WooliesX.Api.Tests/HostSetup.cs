using System.Net.Http;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

namespace WooliesX.Api.Tests
{
    public class HostSetup
    {
        protected readonly HttpClient ApiClient;

        protected HostSetup()
        {
            var host = WebHost.CreateDefaultBuilder().UseStartup<Startup>();
            ApiClient = new TestServer(host).CreateClient();
        }
    }
}