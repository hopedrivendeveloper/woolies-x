using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using Moq;
using WooliesX.Common;
using WooliesX.Services.Repos;
using Xunit;

namespace WooliesX.Services.UnitTests
{
    public class ProductServiceTests
    {

        private readonly Mock<IProductRepository> _productRepositoryMock;
        private readonly Mock<IShopperHistoryRepository> _shopperHistoryMock;
        private readonly ProductsService _subject;
        
        private readonly Fixture _fixture;
        
        public ProductServiceTests()
        {
            _productRepositoryMock = new Mock<IProductRepository>();
            _shopperHistoryMock = new Mock<IShopperHistoryRepository>();
            _subject = new ProductsService(_productRepositoryMock.Object, _shopperHistoryMock.Object);
            
            _fixture = new Fixture();
        }
        
        [Theory]
        [InlineData(SortOption.Ascending)]
        [InlineData(SortOption.Descending)]
        [InlineData(SortOption.Low)]
        [InlineData(SortOption.High)]
        public async Task FindProducts_ReturnsOrderedProducts(SortOption sortOption)
        {
            var products = _fixture.CreateMany<Product>(50).ToList();
            _productRepositoryMock.Setup(m => m.GetProductsAsync()).ReturnsAsync(products);

            var sortedProducts = await _subject.FindProducts(sortOption);

            VerifySortOrder(sortedProducts, products, sortOption);
        }

        private void VerifySortOrder(IEnumerable<Product> sorted, IEnumerable<Product> given, SortOption sortParameter)
        {
            switch (sortParameter)
            {
                case SortOption.Ascending:
                    sorted.Should().BeEquivalentTo(given.OrderBy(p=>p.Name), s => s.WithStrictOrdering());
                    break;
                case SortOption.Descending:
                    sorted.Should().BeEquivalentTo(given.OrderByDescending(p=>p.Name), s => s.WithStrictOrdering());
                    break;
                case SortOption.Low:
                    sorted.Should().BeEquivalentTo(given.OrderBy(p=>p.Price), s => s.WithStrictOrdering());
                    break;
                case SortOption.High:
                    sorted.Should().BeEquivalentTo(given.OrderByDescending(p=>p.Price), s => s.WithStrictOrdering());
                    break;
            }
        }
    }
}