using System.Collections.Generic;

namespace WooliesX.Common
{
    public class Trolley
    {
        public List<Product> Products { get; set; }
        public List<Item> Quantities { get; set; }
        public List<Special> Specials { get; set; }
    }

    public class Special
    {
        public List<Item> Quantities { get; set; }
        public decimal Total { get; set; }
    }
}