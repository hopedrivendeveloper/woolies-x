namespace WooliesX.Common
{
    public class User
    {
        public string Name { get; set; }
        public string Token { get; set; }
    }
}