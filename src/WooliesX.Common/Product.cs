namespace WooliesX.Common
{
    public class Product : Item
    {
        public decimal Price { get; set; }
    }

    public class Item
    {
        public string Name { get; set; }
        public double Quantity { get; set; }
    }
}