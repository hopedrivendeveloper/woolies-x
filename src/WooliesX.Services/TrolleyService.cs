using System;
using System.Linq;
using WooliesX.Common;

namespace WooliesX.Services
{
    public class TrolleyService : ITrolleyService
    {
        public decimal CalculateTotal(Trolley trolley)
        {
            var totalSum = trolley.Quantities.Sum(q=>(decimal)q.Quantity * 
                                                     trolley.Products.First(p=>p.Name.Equals(q.Name, StringComparison.OrdinalIgnoreCase)).Price
            );
            
            foreach (var trolleySpecial in trolley.Specials)
            {
                var conditionsSatisfied = false;
                var sumNotOnSpecial = 0m;
                foreach (var specialQuantity in trolleySpecial.Quantities)
                {
                    var quantityNeeded = trolley.Quantities.FirstOrDefault(p =>
                        p.Name.Equals(specialQuantity.Name, StringComparison.OrdinalIgnoreCase));
                    if (quantityNeeded == null || quantityNeeded.Quantity < specialQuantity.Quantity)
                    {
                        conditionsSatisfied = false;
                        break;
                    }

                    conditionsSatisfied = true;
                    sumNotOnSpecial = sumNotOnSpecial + (decimal)(quantityNeeded.Quantity - specialQuantity.Quantity) *
                        trolley.Products.First(p => p.Name.Equals(specialQuantity.Name, StringComparison.OrdinalIgnoreCase)).Price;
                }

                totalSum = conditionsSatisfied ? Math.Min(totalSum, sumNotOnSpecial + trolleySpecial.Total) : totalSum;
            }

            return totalSum;
        }
    }

    public interface ITrolleyService
    {
        decimal CalculateTotal(Trolley trolley);
    }
}