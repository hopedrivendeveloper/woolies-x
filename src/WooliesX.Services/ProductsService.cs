using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.Common;
using WooliesX.Services.Repos;

namespace WooliesX.Services
{
    public class ProductsService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly IShopperHistoryRepository _shoppingHistoryRepo;

        public ProductsService(IProductRepository productRepository, IShopperHistoryRepository shoppingHistoryRepo)
        {
            _productRepository = productRepository;
            _shoppingHistoryRepo = shoppingHistoryRepo;
        }

        public async Task<IEnumerable<Product>> FindProducts(SortOption sortOption = SortOption.Recommended)
        {
            var products = await _productRepository.GetProductsAsync();

            return await ApplySorting(sortOption, products);
        }

        private async Task<IEnumerable<Product>> ApplySorting(SortOption sortOption, IEnumerable<Product> products)
        {
            switch (sortOption)
            {
                case SortOption.Ascending:
                    return products.OrderBy(p => p.Name);
                case SortOption.Descending:
                    return products.OrderByDescending(p => p.Name);
                case SortOption.High:
                    return products.OrderByDescending(p => p.Price);
                case SortOption.Low:
                    return products.OrderBy(p => p.Price);
                default:
                    return await SortByRecommneded(products);
            }
        }

        private async Task<IEnumerable<Product>> SortByRecommneded(IEnumerable<Product> products)
        {
            var recommended = await _shoppingHistoryRepo.GetHistoryAsync();
            
            var popularProducts = recommended.SelectMany(h => h.Products).GroupBy(p => p.Name).Select(g => new
            {
                Name = g.Key,
                Total = g.Sum(i => i.Quantity)
            }).OrderByDescending(_ => _.Total).ToList();
            var orderedEnumerable = products.OrderBy(p =>
            {
                var index = popularProducts.FindIndex(r =>
                    r.Name.Equals(p.Name, StringComparison.OrdinalIgnoreCase));
                return index == -1 ? int.MaxValue : index;
            }).ToList();
            
            return orderedEnumerable;
        }
    }
    

    public interface IProductService
    {
        public Task<IEnumerable<Product>> FindProducts(SortOption sortOption);
    }
}