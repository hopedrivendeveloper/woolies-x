using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using WooliesX.Common;

namespace WooliesX.Services.Repos
{
    public interface IShopperHistoryRepository
    {
        Task<IEnumerable<ShopperHistory>> GetHistoryAsync();
    }

    public class ShopperHistoryRepository : IShopperHistoryRepository
    {
        private readonly HttpClient _httpClient;
        private readonly IOptions<AppConfig> _options;
        public ShopperHistoryRepository(IHttpClientFactory factory, IOptions<AppConfig> options)
        {
            _options = options;
            _httpClient = factory.CreateClient();
        }
        
        public async Task<IEnumerable<ShopperHistory>> GetHistoryAsync()
        {
            var httpResponse = await _httpClient.GetAsync(
                string.Format(_options.Value.Endpoints.ShopperHistory, _options.Value.User.Token));

            return await httpResponse.Content.ReadFromJsonAsync<IEnumerable<ShopperHistory>>();
        }
        
    }

    public class ShopperHistory
    {
        public IEnumerable<Product> Products { get; set; }
    }
}