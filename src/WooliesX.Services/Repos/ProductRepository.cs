using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using WooliesX.Common;

namespace WooliesX.Services.Repos
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> GetProductsAsync();
    }

    public class ProductRepository : IProductRepository
    {
        private readonly HttpClient _httpClient;
        private readonly IOptions<AppConfig> _options;

        public ProductRepository(IHttpClientFactory factory, IOptions<AppConfig> options)
        {
            _options = options;
            _httpClient = factory.CreateClient();
        }

        public async Task<IEnumerable<Product>> GetProductsAsync()
        {
            var httpResponse = await _httpClient.GetAsync(
                string.Format(_options.Value.Endpoints.Products, _options.Value.User.Token));

            var products = await httpResponse.Content.ReadFromJsonAsync<IEnumerable<Product>>();
            return products;
        }
        
        
    }
}