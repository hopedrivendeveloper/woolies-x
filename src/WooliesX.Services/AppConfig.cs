using WooliesX.Common;

namespace WooliesX.Services
{
    public class AppConfig
    {
        public EndpointsConfig Endpoints { get; set; }
        public User User { get; set; }
    }

    public class EndpointsConfig
    {
        public string ShopperHistory { get; set; }
        public string Products { get; set; }
    }
}