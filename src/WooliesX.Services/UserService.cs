﻿using Microsoft.Extensions.Options;
using WooliesX.Common;

namespace WooliesX.Services
{
    public class UserService : IUserService
    {
        private readonly IOptions<AppConfig> _options;

        public UserService(IOptions<AppConfig> options)
        {
            _options = options;
        }

        public User FindUser()
        {
            return _options.Value.User;
        }
    }

    public interface IUserService
    {
        public User FindUser();
    }

    
}