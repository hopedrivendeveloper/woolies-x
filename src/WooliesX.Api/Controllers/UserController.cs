using Microsoft.AspNetCore.Mvc;
using WooliesX.Common;
using WooliesX.Services;

namespace WooliesX.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public User Get()
        {
            return _userService.FindUser();
        }
    }
}