using Microsoft.AspNetCore.Mvc;
using WooliesX.Common;
using WooliesX.Services;

namespace WooliesX.Api.Controllers
{
    [ApiController]
    public class TrolleyController : ControllerBase
    {
        private readonly ITrolleyService _trolleyService;

        public TrolleyController(ITrolleyService trolleyService)
        {
            _trolleyService = trolleyService;
        }

        [HttpPost]
        [Route("trolleyTotal")]
        public decimal Total([FromBody] Trolley trolley)
        {
            return _trolleyService.CalculateTotal(trolley);
        }
    }
}