using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WooliesX.Common;
using WooliesX.Services;

namespace WooliesX.Api.Controllers
{
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        // GET
        [HttpGet]
        [Route("sort")]
        public Task<IEnumerable<Product>> SortProducts([FromQuery]SortOption sortOption = SortOption.Recommended)
        {
            return _productService.FindProducts(sortOption);
        }
    }
}